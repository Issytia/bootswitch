/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::collections::HashMap;
use std::process::exit;

extern crate ucs2;

use efivar::efi::{VariableFlags, VariableName};
#[cfg(feature = "notify")]
use notify_rust::Notification;
#[cfg(unix)]
use privdrop::PrivDrop;
#[cfg(unix)]
use users::{get_current_username, get_effective_uid};
#[cfg(windows)]
use privilege;

fn main() {
	if std::env::args().len() <= 1 {
		eprintln!("please specify entries to cycle");
		exit(1);
	}

	#[cfg(windows)]
	if !privilege::user::privileged() {
		let mut cmd = privilege::runas::Command::new(std::env::args().next().unwrap());
		cmd.args(&std::env::args().skip(1).collect::<Vec<String>>());

		match cmd.run() {
			Ok(_) => return,
			Err(e) => {
				eprintln!("execute failed: {e}");
				exit(1);
			}
		}
	}

	let mut efi = efivar::system();

	let mut entries: HashMap<u16, String> = HashMap::new();
	let mut order: Vec<u16> = vec![];
	let mut order_flags = VariableFlags::empty();
	let mut order_name = VariableName::new("BootOrder");
	let mut buf = vec![0u8; 4096];

	for name in efi.get_var_names().unwrap_or_else(|e| { eprintln!("failed to enumerate efi variables: {e}"); exit(1); }) {
		if name.short_name() == "BootOrder" {
			order_name = name.clone();
			match efi.read(&name, &mut buf) {
				Ok((s, f)) => { order.extend(to_u16(&buf[.. s])); order_flags = f; },
				Err(e) => {
					eprintln!("failed to read boot order: {e}");
					exit(1);
				}
			}
		} else {
			match name.short_name().strip_prefix("Boot") {
				Some(s) => match s.parse::<u16>() {
					Ok(u) => match efi.read(&name, &mut buf) {
						Ok((_, _)) => match ucs2::decode(to_u16(&buf[6 ..]).as_slice(), &mut buf) {
							Ok(_) => match buf.iter().position(|&b| b == 0) {
								Some(i) => { entries.insert(u, String::from_utf8(buf[.. i].to_owned()).unwrap()); },
								None => panic!("no null byte")
							},
							Err(e) => {
								eprintln!("ucs2 decode failed: {e:?}");
								exit(1);
							}
						},
						Err(e) => {
							eprintln!("failed to read boot entry: {e}");
							exit(1);
						}
					},
					Err(_) => () // not a boot entry
				},
				None => () // not a boot entry
			}
		}
	}

	{
		let mut cycle: Vec<&u16> = vec![];
		for s in std::env::args().skip(1) {
			let matches: Vec<(&u16, &String)> = entries.iter()
				.filter(|&i| { i.1.to_lowercase().starts_with(s.to_lowercase().as_str()) })
				.collect();
			if matches.len() == 0 {
				eprintln!("no entry found for {s}");
				exit(1);
			} else if matches.len() > 1 {
				eprintln!("multiple entries match {s}");
				for i in matches {
					eprintln!("  {}", i.1);
				}
				exit(1);
			} else if cycle.iter().any(|j| *j == matches[0].0) {
				eprintln!("duplicate entry: {}", s);
				exit(1);
			} else {
				cycle.push(matches[0].0);
			}
		}
		if cycle.len() == 0 {
			eprintln!("please specify entries to cycle");
			exit(1);
		}

		println!("old order:");
		for i in &order {
			if entries.contains_key(i) {
				println!("  {}", entries[i]);
			}
		}

		match cycle.iter().position(|i| *i == &order[0]) {
			Some(i) => if i != cycle.len() - 1 { cycle.rotate_left(i + 1) },
			None => ()
		}

		order.retain(|i| { !cycle.iter().any(|j| { *j == i }) });
		order.splice(0 .. 0, cycle.drain(0 ..).map(|i| { *i }));
	}

	println!("new order:");
	for i in &order {
		if entries.contains_key(i) {
			println!("  {}", entries[i]);
		}
	}

	let result = efi.write(&order_name, order_flags, from_u16(&order).as_slice());

	#[cfg(unix)]
	if get_effective_uid() == 0 {
		PrivDrop::default()
			.user(get_current_username().expect("failed to get current username"))
			.apply()
			.expect("failed to drop privileges");
	}

	match result {
		Ok(()) => {
			println!("write success");
			#[cfg(feature = "notify")]
			Notification::new()
				.summary("bootswitch")
				.body(format!("Next boot: {}", entries[&order[0]]).as_str())
				.show()
				.unwrap_or_else(|e| {
					eprintln!("failed to show notification: {e}");
					exit(2);
				});
		},
		Err(e) => {
			eprintln!("write failed: {e}");
			#[cfg(feature = "notify")]
			Notification::new()
				.summary("bootswitch")
				.body(format!("Failed to change boot order: {e}").as_str())
				.show()
				.unwrap_or_else(|e| {
					eprintln!("failed to show notification: {e}");
					exit(2);
				});
			exit(1);
		}
	}
}

fn to_u16(buf: &[u8]) -> Vec<u16> {
	assert_eq!(buf.len() % 2, 0);
	let mut r = vec![0u16; buf.len() / 2];
	for i in (0 .. buf.len()).step_by(2) {
		r[i / 2] = u16::from_le_bytes(buf[i .. i+2].try_into().expect("+2 is incorrect?"));
	}
	r
}

fn from_u16(buf: &[u16]) -> Vec<u8> {
	let mut r = vec![0u8; buf.len() * 2];
	for i in 0 .. buf.len() {
		r.splice(i * 2 .. i * 2 + 2, u16::to_le_bytes(buf[i]));
	}
	r
}

#[cfg(test)]
mod tests {
	#[test]
	fn to_u16() {
		let left = [0x10u8, 0x00, 0x14, 0x00, 0x18, 0x00, 0x00, 0x18];
		let right = [0x0010u16, 0x0014, 0x0018, 0x1800];

		assert_eq!(super::to_u16(&left), right);
	}

	#[test]
	fn from_u16() {
		let left = [0x0010u16, 0x0014, 0x0018, 0x1800];
		let right = [0x10u8, 0x00, 0x14, 0x00, 0x18, 0x00, 0x00, 0x18];

		assert_eq!(super::from_u16(&left), right)
	}

	#[test]
	fn order_consistency() {
		let efi = efivar::system();

		let mut raw_order: Vec<u8> = vec![];
		let mut order: Vec<u16> = vec![];
		let mut buf = vec![0u8; 512];

		for var in efi.get_var_names().unwrap() {
			if var.short_name() == "BootOrder" {
				let (s, _) = efi.read(&var, &mut buf).unwrap();
				raw_order = buf[.. s].to_owned();
				order.extend(super::to_u16(&buf[.. s]));
			}
		}

		assert_eq!(raw_order, super::from_u16(&order));
	}

	#[test]
	fn name_consistency() {
		let efi = efivar::system();

		let mut name = super::VariableName::new("BootOrder");

		for var in efi.get_var_names().unwrap() {
			if var.short_name() == "BootOrder" {
				name = var;
			}
		}

		assert_eq!(name, super::VariableName::new("BootOrder"));
	}

}
